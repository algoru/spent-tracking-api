package entity

import "time"

type Transaction struct {
	ID        uint       `json:"id"`
	Tag       Tag        `json:"tag,omitempty"`
	Value     float64    `json:"value"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at,omitempty"`
}
