package ginserver

import (
	"fmt"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	tagservice "gitlab.com/algoru/spent-tracking-api/service/tag"
	txservice "gitlab.com/algoru/spent-tracking-api/service/transaction"
)

type GinServer struct {
	tagService tagservice.Service
	txService  txservice.Service
	port       uint16
	router     *gin.Engine
}

func NewGinServer(port uint16) GinServer {
	router := gin.Default()
	router.Use(cors.Default())

	return GinServer{
		port:   port,
		router: router,
	}
}

func (gs *GinServer) InitializeRoutes() {
	apiGroup := gs.router.Group("/api")
	RegisterTagRoutes(apiGroup, gs.tagService)
	RegisterTransactionRoutes(apiGroup, gs.txService)
}

func (gs *GinServer) Start() error {
	return gs.router.Run(fmt.Sprintf(":%d", gs.port))
}

func (gs *GinServer) SetTagService(ts tagservice.Service) {
	gs.tagService = ts
}

func (gs *GinServer) SetTransactionService(ts txservice.Service) {
	gs.txService = ts
}
