package txrepository

import (
	"time"

	tagrepository "gitlab.com/algoru/spent-tracking-api/repository/tag"
)

type (
	Transaction struct {
		ID        uint              `gorm:"column:id;primary_key"`
		TagID     uint              `gorm:"column:tag_id"`
		Tag       tagrepository.Tag `gorm:"association_foreignkey:TagID;foreignkey:ID"`
		Value     float64
		CreatedAt time.Time
		UpdatedAt time.Time
		DeletedAt *time.Time `sql:"index"`
	}

	TransactionFilter struct {
		Offset, Limit uint
		TagID         uint
		From          *time.Time
		To            *time.Time
	}
)

func (Transaction) TableName() string {
	return "transaction"
}
