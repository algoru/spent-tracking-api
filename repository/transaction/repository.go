package txrepository

type Repository interface {
	GetTransactionErrorMessage(uint, error) (int, string)
	GetTransaction(uint) (*Transaction, error)
	GetTransactions(TransactionFilter) ([]Transaction, error)
	SaveTransaction(uint, float64) (*Transaction, error)
	EditTransaction(uint, *Transaction) (*Transaction, error)
	DeleteTransaction(uint) error
}
