package rest

import (
	tagservice "gitlab.com/algoru/spent-tracking-api/service/tag"
	txservice "gitlab.com/algoru/spent-tracking-api/service/transaction"
)

type Port interface {
	SetTagService(tagservice.Service)
	SetTransactionService(txservice.Service)
	InitializeRoutes()
	Start() error
}
