package storage

import (
	tagrepository "gitlab.com/algoru/spent-tracking-api/repository/tag"
	txrepository "gitlab.com/algoru/spent-tracking-api/repository/transaction"
)

type Port interface {
	tagrepository.Repository
	txrepository.Repository
}
