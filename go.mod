module gitlab.com/algoru/spent-tracking-api

go 1.14

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jinzhu/gorm v1.9.15
	github.com/joho/godotenv v1.3.0
	github.com/microcosm-cc/bluemonday v1.0.3
	github.com/stretchr/testify v1.4.0
)
