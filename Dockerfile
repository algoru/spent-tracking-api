FROM golang:1.14

WORKDIR /go/src/gitlab.com/algoru/spent-track-api
COPY . .

RUN go mod download
RUN go build

CMD ["./spent-track-api"]