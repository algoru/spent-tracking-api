package main

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/algoru/spent-tracking-api/infrastructure"
	"gitlab.com/algoru/spent-tracking-api/infrastructure/adapters/rest/ginserver"
	"gitlab.com/algoru/spent-tracking-api/infrastructure/adapters/storage/mariadb"
)

func main() {
	if err := godotenv.Load(); err != nil {
		log.Fatal(err)
	}

	// sqlitePort, err := sqlite.NewStorage()
	// if err != nil {
	// 	log.Fatal(err)
	// }

	mariadbPort, err := mariadb.NewStorage()
	if err != nil {
		log.Fatal(err)
	}

	ginServerPort := ginserver.NewGinServer(8080)

	// Storage, Web, Mailing, MQs, Logger, etc...
	appConfig := infrastructure.ApplicationConfiguration{
		StoragePort: mariadbPort,
		RestPort:    &ginServerPort,
	}

	if err := infrastructure.StartApplication(appConfig); err != nil {
		log.Fatal(err)
	}
}
