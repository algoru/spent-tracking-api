package tagservice

import "github.com/microcosm-cc/bluemonday"

type Tag struct {
	Name string `json:"name"`
}

func (t *Tag) Sanitize() {
	pol := bluemonday.StrictPolicy()
	t.Name = pol.Sanitize(t.Name)
}
