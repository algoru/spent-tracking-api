package tagrepository

type Repository interface {
	GetTagErrorMessage(uint, error) (int, string)
	GetTag(uint) (*Tag, error)
	GetTags(uint, uint) ([]Tag, error)
	GetMostProfitableTag() (*Tag, error)
	GetLessProfitableTag() (*Tag, error)
	SaveTag(string) (*Tag, error)
	EditTag(uint, *Tag) (*Tag, error)
	DeleteTag(uint) error
	RestoreTag(uint) error
}
