CREATE TABLE `transaction` (
	`id`         INTEGER  NOT NULL PRIMARY KEY,
	`tag_id`     INTEGER  NOT NULL,
	`value`      REAL     NOT NULL,
	`created_at` DATETIME NOT NULL,
   	`updated_at` DATETIME NOT NULL,
   	`deleted_at` DATETIME,
   	FOREIGN KEY(`tag_id`) REFERENCES tag(`id`)
);
