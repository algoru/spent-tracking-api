package infrastructure

import (
	"gitlab.com/algoru/spent-tracking-api/infrastructure/adapters/rest"
	"gitlab.com/algoru/spent-tracking-api/infrastructure/adapters/storage"
	tagservice "gitlab.com/algoru/spent-tracking-api/service/tag"
	txservice "gitlab.com/algoru/spent-tracking-api/service/transaction"
)

type ApplicationConfiguration struct {
	StoragePort storage.Port
	RestPort    rest.Port
}

func StartApplication(ac ApplicationConfiguration) error {
	tagService := tagservice.NewService(ac.StoragePort)
	txService := txservice.NewService(ac.StoragePort)

	ac.RestPort.SetTagService(tagService)
	ac.RestPort.SetTransactionService(txService)
	ac.RestPort.InitializeRoutes()

	return ac.RestPort.Start()
}
