package ginserver

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	txservice "gitlab.com/algoru/spent-tracking-api/service/transaction"
)

func RegisterTransactionRoutes(group *gin.RouterGroup, ts txservice.Service) {
	transactions := group.Group("/transactions")
	transactions.GET("/:id", getTransaction(ts))
	transactions.GET("", getTransactions(ts))
	transactions.POST("", createTransaction(ts))
	transactions.PUT("/:id", editTransaction(ts))
	transactions.DELETE("/:id", deleteTransaction(ts))
}

func getTransaction(ts txservice.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		txID, err := strconv.ParseUint(c.Param("id"), 10, 64)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid transaction id"})
			return
		}

		tx, err := ts.GetTransaction(uint(txID))
		if err != nil {
			statusCode, message := ts.GetTransactionErrorMessage(uint(txID), err)
			c.AbortWithStatusJSON(statusCode, gin.H{"error": message})
			return
		}

		c.JSON(http.StatusOK, tx)
	}
}

func getTransactions(ts txservice.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		tf := txservice.TransactionFilter{}

		if err := c.BindQuery(&tf); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		transactions, err := ts.GetTransactions(tf)
		if err != nil {
			statusCode, message := ts.GetTransactionErrorMessage(0, err)
			c.AbortWithStatusJSON(statusCode, gin.H{"error": message})
			return
		}

		c.JSON(http.StatusOK, transactions)
	}
}

func createTransaction(ts txservice.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		tx := txservice.Transaction{}
		if err := c.ShouldBindJSON(&tx); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid json body"})
			return
		}

		savedTx, err := ts.SaveTransaction(tx)
		if err != nil {
			statusCode, message := ts.GetTransactionErrorMessage(0, err)
			c.AbortWithStatusJSON(statusCode, gin.H{"error": message})
			return
		}

		c.JSON(http.StatusCreated, savedTx)
	}
}

func editTransaction(ts txservice.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		txID, err := strconv.ParseUint(c.Param("id"), 10, 64)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid transaction id"})
			return
		}

		tx := txservice.Transaction{}
		if err := c.ShouldBindJSON(&tx); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid json body"})
			return
		}

		updatedTx, err := ts.EditTransaction(uint(txID), tx)
		if err != nil {
			statusCode, message := ts.GetTransactionErrorMessage(uint(txID), err)
			c.AbortWithStatusJSON(statusCode, gin.H{"error": message})
			return
		}

		c.JSON(http.StatusOK, updatedTx)
	}
}

func deleteTransaction(ts txservice.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		txID, err := strconv.ParseUint(c.Param("id"), 10, 64)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid transaction id"})
			return
		}

		deletedTx, err := ts.DeleteTransaction(uint(txID))
		if err != nil {
			statusCode, message := ts.GetTransactionErrorMessage(uint(txID), err)
			c.AbortWithStatusJSON(statusCode, gin.H{"error": message})
			return
		}

		c.JSON(http.StatusOK, deletedTx)
	}
}
