package tagrepository

import "time"

type Tag struct {
	ID        uint `gorm:"column:id;primary_key"`
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}

func (Tag) TableName() string {
	return "tag"
}
