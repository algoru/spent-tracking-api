CREATE TABLE `transaction` (
	`id`         BIGINT AUTO_INCREMENT,
	`tag_id`     BIGINT    NOT NULL,
	`value`      DOUBLE    NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`deleted_at` TIMESTAMP     NULL,
	PRIMARY KEY(`id`),
	FOREIGN KEY(`tag_id`) REFERENCES tag(`id`)
);